const basePath = '/helados-in-dh-group'

module.exports = {
  pathPrefix: basePath,
  siteMetadata: {
    title: `Helados`,
    description: `A website for Mith301.`,
    author: `Maria Agustina Ryckeboer and Sidra Nadeem`
  },
  plugins: [
    `gatsby-plugin-material-ui`,
    `gatsby-theme-ceteicean`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/tei`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/pages`,
        name: `html`,
      },
    },
  ],
}
