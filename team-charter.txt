﻿Team charter / Contrato de equipo
Last updated / Última actualización: 2021-09-26  
Group name / Nombre del Equipo: Ice Cream / Helados 


Roles / Roles
Technical leader / Líder técnica : Sidra Nadeem. 


Submitter / Encargada de envíos: María Agustina Ryckeboer


Team Members / Integrantes del equipo
* Sidra Nadeem, She/Her/Hers, US, Technical Leader 


* María Agustina Ryckeboer, She/her/hers, Arg, Submitter


Goals / Objetivos


We want to develop skills regarding GitLab and Visual Studio Code. We want to be able to create digital humanitarian projects that are communicated effectively to our audience. We want to create a website at the end of this course and know how to create more websites in the future. We will effectively collaborate with each other to work on the assigned projects and communicate with each other regarding issues with our project. 


We will write notes on what we have learned and completed in GitLab and Visual Studio Codes so we know where we stopped and have to continue during the next class. We will also write down notes on coding we do not understand so we can learn about it before using it for the future. We want to achieve a successful project at the end of this course and successfully be able to work on GitLab and VS Code in the future. 


Buscamos desarrollar las capacidades necesarias para trabajar con GitLab y Visual Studio Code. Queremos ser capaces de crear proyectos relacionados a las Humanidades Digitales que logren ser comunicados eficientemente a nuestra audiencia. Buscamos lograr elaborar un sitio web al final de este curso y poder ser capaces de crear más a futuro. Colaboraremos eficientemente entre nosotras al trabajar en el proyecto asignado, comunicándonos sobre cualquier inconveniente que surja en el mismo. 


Escribiremos notas de lo que hemos aprendido y completado en GitLab y VS Code avisando donde nos hemos detenido para así poder saber donde continuar la siguiente clase. Anotaremos y avisaremos cuando no entendamos algún código para así poder aprenderlo antes de utilizarlo a futuro. Nuestra meta final es lograr elaborar un proyecto exitoso y poder ser capaces de utilizar Gitlab y VS Code a futuro.




Core Values / Fundamentos


By communicating effectively with each other, we will be able to successfully achieve our goals. Expectations consist of communicating about problems regarding GitLab or VS Code and asking questions on uncertain topics so have a better understanding of our assignments. We will communicate via WhatsApp to discuss anything regarding our project. We will share our schedules with each other so we know when we can be free to work on our assignments. We will try to incorporate both English and Spanish in our communications so we can communicate in new ways that are effective. By communicating effectively through our respective languages and schedules, we will respect each other, become close teammates and friends.  


La base para lograr nuestros objetivos será la comunicación. Esperamos poder comunicarnos cuando surja algún problema con GitLab o VS Code o consultarnos cuando creamos que haya temas que no terminamos de comprender. Nos comunicaremos a través de WhatsApp para discutir todo lo referido a los trabajos. Buscaremos hablar tanto en inglés como en español para lograr una mejor comunicación y mejorar nuestra práctica con los idiomas. Todo esto en un clima de respeto para lograr ser mejores compañeras y amigas.




Team management / Gestión del equipo


Respecting and effectively communicating is how we are going to manage our team in the best way. We have used both Slack and WhatsApp to communicate, WhatsApp being the most effective tool so far. We will try our best to meet virtually at least once a week if needed. To handle decision making, we will establish our objectives and goals to make the best, productive decision regarding our project. If our first decision fails, we would have thought of alternatives to our original decision just to be safe. To resolve conflicts, we will accept there is a conflict to resolve first and think of multiple steps to resolve it in the best way possible by collaborating and communicating successfully. Yes, we will each have specific roles and responsibilities, Sidra being the technical leader and Maria being the Submitter. 




El respeto y la comunicación serán las bases de las relaciones del grupo. Usaremos para comunicarnos Slack y WhatsApp, siendo esta última la más utilizada. Haremos lo posible por juntarnos virtualmente una vez a la semana si es necesario. Para tomar decisiones primero estableceremos nuestros objetivos y metas para poder tomar la mejor decisión en cuanto al proyecto. Si la primera solución no funciona tendremos pensadas alternativas para estar seguras de no tener imprevistos. A la hora de resolver un conflicto, será importante reconocerlo primero para poder pensar los pasos para superarlo, colaborando y comunicándonos. Los roles y responsabilidades definidos por el momento son Sidra como la líder técnica y María como la encargada de los envíos.


Ethos /Ethos


* Group comes first 
* Does not leave work for the last minute
* We strive to remain accountable 


* El grupo es lo primero
* No dejar trabajo sin hacer para último momento
* Mantenernos en contacto